package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.SimpleStrategy;

public interface SimpleStrategyDao {

    List<SimpleStrategy> findAll();

    List<SimpleStrategy> findActive();

    List<SimpleStrategy> findAllByStockTicker(String ticker);

    int save(SimpleStrategy strategy);
}
