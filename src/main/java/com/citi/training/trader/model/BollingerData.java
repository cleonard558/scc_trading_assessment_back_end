package com.citi.training.trader.model;


public class BollingerData {
	 	
		private int totalCount;
	    private double totalPrice;
	    private double avgPrice; 
	    private double stdDev;
	    
	    
	    public BollingerData(int totalCount, double totalPrice, double avgPrice, double stdDev) {
	        this.totalCount = totalCount;
	        this.totalPrice = totalPrice;
	        this.avgPrice = avgPrice;
	        this.stdDev = stdDev;
	    }


		public int getTotalCount() {
			return totalCount;
		}


		public void setTotalCount(int totalCount) {
			this.totalCount = totalCount;
		}


		public double getTotalPrice() {
			return totalPrice;
		}


		public void setTotalPrice(double totalPrice) {
			this.totalPrice = totalPrice;
		}


		public double getAvgPrice() {
			return avgPrice;
		}


		public void setAvgPrice(double avgPrice) {
			this.avgPrice = avgPrice;
		}


		public double getStdDev() {
			return stdDev;
		}


		public void setStdDev(double stdDev) {
			this.stdDev = stdDev;
		}


		@Override
		public String toString() {
			return "BollingerData [totalCount=" + totalCount + ", totalPrice=" + totalPrice + ", avgPrice=" + avgPrice
			        + ", stdDev=" + stdDev + "]";
		}

	    
		
}
