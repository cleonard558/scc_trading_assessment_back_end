package com.citi.training.trader.rest;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.service.SimpleStrategyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST Controller for {@link com.citi.training.trader.model.SimpleStrategy} resource.
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/api/strategy}")
public class SimpleStrategyController {

    private static final Logger logger =
                    LoggerFactory.getLogger(SimpleStrategyController.class);

    @Autowired
    private SimpleStrategyService simpleStrategyService;

    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<SimpleStrategy> findAll() {
        logger.debug("findAll()");
        return simpleStrategyService.findAll();
    }

    @RequestMapping(value="/{ticker}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public List<SimpleStrategy> findAllByStockTicker(@PathVariable String ticker) {
        logger.debug("findAllByStockTicker()");
        return simpleStrategyService.findAllByStockTicker(ticker);
    }

    @RequestMapping(value="/active", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public List<SimpleStrategy> findActive() {
        logger.debug("findActive()");
        return simpleStrategyService.findActive();
    }


    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<SimpleStrategy> create(@RequestBody SimpleStrategy strategy) {
        logger.debug("create(" + strategy + ")");
        strategy.setId(simpleStrategyService.save(strategy));
        logger.debug("created new strategy: " + strategy);
        return new ResponseEntity<>(strategy, HttpStatus.CREATED);
    }

}
