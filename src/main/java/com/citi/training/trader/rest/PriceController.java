package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.StockService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/api/price}")
public class PriceController {

	private static final Logger logger = LoggerFactory.getLogger(PriceController.class);
  
	@Autowired
	private PriceService priceService;
	
	@Autowired
	private StockService stockService;
	
	 @RequestMapping(value="/{ticker}", method=RequestMethod.GET,
	 produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Price> findAll(@PathVariable String ticker) {
		logger.debug("findAll()");
		Stock tmpStock = new Stock(-1,ticker);
		return priceService.findAll(tmpStock);
	}

	@RequestMapping(value="/max", method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Price> findMaxPrice() {
		logger.debug("findMaxPrice()");
		return priceService.findMaxPrice();
	}
	 
	 @RequestMapping(value="/{ticker}/{limit}", method=RequestMethod.GET,
			 produces=MediaType.APPLICATION_JSON_VALUE)
	 public List<Price> findLatest(@PathVariable String ticker, @PathVariable int limit )
	 {         logger.debug("findById(" + ticker + ", " + limit +")");
	 Stock tickerMatch = stockService.findByTicker(ticker); 
	 return priceService.findLatest(tickerMatch, limit);
	 }
}
