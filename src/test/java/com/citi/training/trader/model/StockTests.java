package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StockTests {

	private int testId = 12;
	private String testTicker = "APPL";

	@Test
	public void test_Stock_constructor() {
		Stock testStock = new Stock(testId, testTicker);
		assertEquals(testId, testStock.getId());
		assertEquals(testTicker, testStock.getTicker());
	}

	@Test
	public void test_Stock_toString() {
		String testString = new Stock(testId, testTicker).toString();
		assertTrue(testString.contains((new Integer(testId)).toString()));
		assertTrue(testString.contains(String.valueOf(testTicker)));
	}
}
