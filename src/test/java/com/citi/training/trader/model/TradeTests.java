package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.Date;

import org.junit.Test;

import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

public class TradeTests {

	private int testId = 12;
	private double testPrice = 1.99;
	private int testSize = 100;
	private TradeType testTradeType = TradeType.BUY;
	private TradeState testTradeState = TradeState.FILLED;
	private LocalDateTime testLastStateChange = LocalDateTime.of(2019, 2, 13, 15, 56);
	private boolean testAccountedFor = false;
	private SimpleStrategy testStrategy = new SimpleStrategy(30, new Stock(19, "MSFT"), 555, 1.5, 0, 50.3, 2.7,
			new Date(), 16);

	@Test
	public void test_Trade_constructor() {
		Trade testTrade = new Trade(testId, testPrice, testSize, testTradeType, testTradeState, testLastStateChange,
				testAccountedFor, testStrategy);
		assertEquals(new Integer(testId), testTrade.getId());
		assertEquals(testPrice, testTrade.getPrice(), 0.0001);
		assertEquals(testSize, testTrade.getSize());
		assertEquals(testTradeType, testTrade.getTradeType());
		assertEquals(testTradeState, testTrade.getState());
		assertEquals(testLastStateChange, testTrade.getLastStateChange());
		assertEquals(testAccountedFor, testTrade.getAccountedFor());
		assertEquals(testStrategy, testTrade.getStrategy());
	}

	@Test
	public void test_Trade_toString() {
		String testString = new Trade(testId, testPrice, testSize, testTradeType, testTradeState, testLastStateChange,
				testAccountedFor, testStrategy).toString();
		assertTrue(testString.contains((new Integer(testId)).toString()));
		assertTrue(testString.contains(String.valueOf(testPrice)));
		assertTrue(testString.contains(String.valueOf(testSize)));
		assertTrue(testString.contains(String.valueOf(testTradeType)));
		assertTrue(testString.contains(String.valueOf(testTradeState)));
		assertTrue(testString.contains(String.valueOf(testLastStateChange)));
		assertTrue(testString.contains(String.valueOf(testAccountedFor)));
		// assertTrue(testString.contains(String.valueOf(testStrategy)));
	}
}
