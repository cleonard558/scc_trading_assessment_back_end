package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class PriceTests {

	private int testId = 23;
	private Stock testStock = new Stock(1, "AAPL");
	private double testPriceDouble = 1.98;
	private Date testRecordedAt = new Date();

	@Test
	public void test_Price_constructor() {
		Price testPrice = new Price(testId, testStock, testPriceDouble, testRecordedAt);
		assertEquals(testId, testPrice.getId());
		assertEquals(testStock, testPrice.getStock());
		assertEquals(testPriceDouble, testPrice.getPrice(), 0.0001);
		assertEquals(testRecordedAt, testPrice.getRecordedAt());

	}

	@Test
	public void test_Price_toString() {
		String testString = new Price(testId, testStock, testPriceDouble, testRecordedAt).toString();
		assertTrue(testString.contains((new Integer(testId)).toString()));
		assertTrue(testString.contains(String.valueOf(testStock)));
		assertTrue(testString.contains(String.valueOf(testPriceDouble)));
		assertTrue(testString.contains(String.valueOf(testRecordedAt)));

	}

}
