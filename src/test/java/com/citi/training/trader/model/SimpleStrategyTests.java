package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class SimpleStrategyTests {

	private int testId = 15;
	private Stock testStock = new Stock(22, "GOGL");
	private int testSize = 150;
	private double testExitProfitLoss = 2.3;
	private int testCurrentPosition = 0;
	private double testLastTradePrice = 30.6;
	private double testProfit = 1.9;
	private Date testStopped = new Date();
	private int testDuration = 15;

	@Test
	public void test_SimpleStrategy_constructor() {
		SimpleStrategy testSimpleStrategy = new SimpleStrategy(testId, testStock, testSize, testExitProfitLoss,
				testCurrentPosition, testLastTradePrice, testProfit, testStopped,testDuration);
		assertEquals(testId, testSimpleStrategy.getId());
		assertEquals(testStock, testSimpleStrategy.getStock());
		assertEquals(testSize, testSimpleStrategy.getSize());
		assertEquals(testExitProfitLoss, testSimpleStrategy.getExitProfitLoss(), 0.0001);
		assertEquals(testCurrentPosition, testSimpleStrategy.getCurrentPosition());
		assertEquals(testLastTradePrice, testSimpleStrategy.getLastTradePrice(), 0.0001);
		assertEquals(testProfit, testSimpleStrategy.getProfit(), 0.0001);
		assertEquals(testStopped, testSimpleStrategy.getStopped());
		assertEquals(testDuration, testSimpleStrategy.getDuration());
	}

	@Test
	public void test_SimpleStrategy_toString() {
		String testString = new SimpleStrategy(testId, testStock, testSize, testExitProfitLoss, testCurrentPosition,
				testLastTradePrice, testProfit, testStopped, testDuration).toString();
		assertTrue(testString.contains((new Integer(testId)).toString()));
		assertTrue(testString.contains(String.valueOf(testStock)));
		assertTrue(testString.contains(String.valueOf(testSize)));
		assertTrue(testString.contains(String.valueOf(testExitProfitLoss)));
		assertTrue(testString.contains(String.valueOf(testCurrentPosition)));
		assertTrue(testString.contains(String.valueOf(testStopped)));
		assertTrue(testString.contains(String.valueOf(testDuration)));

	}

}
