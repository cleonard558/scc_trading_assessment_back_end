package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BollingerDataTests {

	private int testTotalCount = 200;
	private double testTotalPrice = 543.21;
	private double testAvgPrice = 3.5;
	private double testStdDev = 2.1;

	@Test
	public void test_BollingerData_constructor() {
		BollingerData testBollingerData = new BollingerData(testTotalCount, testTotalPrice, testAvgPrice, testStdDev);
		assertEquals(testTotalCount, testBollingerData.getTotalCount());
		assertEquals(testTotalPrice, testBollingerData.getTotalPrice(), 0.001);
		assertEquals(testAvgPrice, testBollingerData.getAvgPrice(), 0.001);
		assertEquals(testStdDev, testBollingerData.getStdDev(), 0.001);
	}

	@Test
	public void test_BollingerData_toString() {
		String testString = new BollingerData(testTotalCount, testTotalPrice, testAvgPrice, testStdDev).toString();
		assertTrue(testString.contains((new Integer(testTotalCount)).toString()));
		assertTrue(testString.contains(String.valueOf(testTotalPrice)));
		assertTrue(testString.contains(String.valueOf(testAvgPrice)));
		assertTrue(testString.contains(String.valueOf(testStdDev)));
	}
}
